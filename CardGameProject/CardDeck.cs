﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
	public class CardDeck
	{
		#region Field Variables
		//declare the list of cards in the deck
		private List<Card> _cardList;
		#endregion

		#region Constructors
		public CardDeck()
		{
			_cardList = new List<Card>();

			CreateCards();
		}
		#endregion

		#region Properties
		//TODO: define a read-only property that returns the number of cards
		//in the _cardList
		public int CardCount
		{
			get { return _cardList.Count; }
		}

		public IEnumerable<Card> Cards
		{
			get { return _cardList; }
		}
		#endregion

		#region Methods
		private void CreateCards()
		{
			//go through each suit and create the cards for it
			for (int iSuit = 1; iSuit <= 4; iSuit++)
			{
				//determine the current suit
				CardSuit suit = (CardSuit)iSuit;

				//go trough all the values of the current suit
				for (byte value = 1; value <= 13; value++)
				{
					//create a card object with the given value and suit
					Card cardObj = new Card(value, suit);

					//add the card to the deck
					_cardList.Add(cardObj);
				}
			}
		}

		public void ShuffleCards()
		{
			//create a randomizer object
			Random randGen = new Random();
			randGen.Next(0, 52);

			//TODO: use the Fisher-Yates shuffle algorithm to randomize the cards
		}
		#endregion
	}
}
