﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
	public struct GameScore
	{
		//Declare the variables that represent the score
		private int _playerScore;

		private int _houseScore;

		public GameScore(int playerScore, int houseScore)
		{
			_playerScore = playerScore;
			_houseScore = houseScore;
			
		}

		//Define two read/write properties for the user and 
		//house score respectively
		public int PlayerScore
		{
			get { return _playerScore; }
			set { _playerScore = value; }
		}

		public int HouseScore
		{
			get { return _houseScore; }
			set { _houseScore = value; }
		}

	}

	public class CardGame
	{
		//Declare the card deck
		private CardDeck _cardDeck;

		//Declare the score for the game
		private GameScore _score;

		//declare the current card played by the house
		private Card _houseCard;

		//declare the current card player by the player
		private Card _playerCard;

		public CardGame()
		{
			_cardDeck = new CardDeck();

			_score = new GameScore(0, 0);

			_playerCard = null;

			_houseCard = null;

		}

		/// <summary>
		/// Read-only property to acces the deck of cards
		/// </summary>
		public CardDeck CardDeck
		{
			get { return _cardDeck; }
		}

		public GameScore Score
		{
			get { return _score; }
		}

		public Card PlayerCard
		{
			get { return _playerCard; }
		}

		public Card HouseCard
		{
			get { return _houseCard; }
		}

		public bool PlayerWins
		{
			get { return _score.PlayerScore > _score.HouseScore; }
		}

		//Implement the property HouseWins
		public bool HouseWins
		{
			get { return _score.HouseScore > _score.PlayerScore; }
		}

		//Implement the property IsOver
		public bool IsOver
		{
			get { return _cardDeck.CardCount == 0; }
		}

		/// <summary>
		/// Shuffles the card in the deck and plays rounds until
		/// the game is over, when all the cards have been played
		/// </summary>
		public void Play()
		{
			//shuffle the cards in the deck
			_cardDeck.ShuffleCards();

			//repeat playing rounds while the game is not over
			while(this.IsOver == false)
			{
				//play the round
				sbyte roundResult = PlayRound();

				//show the result of the round to the user (who won the round)
				ShowRoundResult(roundResult);
			}

			//show the game results and game over
			ShowGameOver();
		}

		/// <summary>
		/// Deal two cards for the user and the house respectively, determine their
		/// ranks and compare the ranks to find out who won, the user or the house
		/// </summary>
		/// <returns>
		///		+ 1: if the user won the round
		///		- 1: if the house won the round
		///		0  : if the round was a draw
		/// </returns>
		public sbyte PlayRound()
		{
			//deal the two cards for the user and the house
			DealCards();

			//determine the rank of the two cards
			byte playerCardRank = DetermineCardRank(_playerCard);
			byte houseCardRank = DetermineCardRank(_houseCard);

			//compare the rank cards to determine who won and update the score
			if (playerCardRank > houseCardRank)
			{
				//player won this round, give them one point
				_score.PlayerScore += 1;
				return 1;

			}
			else if (houseCardRank > playerCardRank)
			{
				//house won this round
				_score.HouseScore += 1;
				return -1;
			}
			else
			{
				//the round was is a draw
				return 0;
			}

		}

		private void ShowRoundResult(sbyte roundResult)
		{
			//TBD: show the result of who won the round,  house or player
		}

		private void ShowGameOver()
		{
			//TBD: let the user know that the game is over
		}
		private void DealCards()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Returns the rank of the card such that Ace has the highest rank in the deck
		/// </summary>
		/// <param name="card"></param>
		/// <returns>
		///  - 14 for Ace cards with the value 1
		///  - value of the card for all other cards
		/// </returns>
		private byte DetermineCardRank(Card card)
		{
			return (card.Value == 1) ? (byte)14 : card.Value;
		}
	}
}
