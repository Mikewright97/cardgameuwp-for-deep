﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
	public enum CardSuit
	{
		Diamonds = 1,
		Clubs,
		Hearts,
		Spades
	}

	/// <summary>
	/// Represents a card game playing card
	/// </summary>
	public class Card
	{
		/// <summary>
		/// The value of the card, 1 -> 13
		/// </summary>
		private byte _value;

		/// <summary>
		/// The suit of the card
		/// </summary>
		private CardSuit _suit;

		public Card(byte value, CardSuit suit)
		{
			_value = value;
			_suit = suit;
		}

		/// <summary>
		/// Accessor and mutator functionality for _value
		/// </summary>
		public byte Value
		{
			get { return _value; }
			set { _value = value; }
		}

		//declare a read/write property _suit
		public CardSuit Suit
		{
			get { return _suit; }
			set { _suit = value; }
		}
		
		/// <summary>
		/// Read-only property that returns the name of the card. The name
		/// of the card is its value for cards 2 - 10 and Jack, Queen, King
		/// and Ace for the face cards.
		/// Example of a "calculated property" that is read-only
		/// </summary>
		public string CardName
		{ 
			get 
			{
				string name;
				switch (_value)
				{
					case 1:
						name = "Ace";
						break;

					case 11:
						name = "Jack";
						break;

					case 12:
						name = "Queen";
						break;

					case 13:
						name = "King";
						break;

					default:
						//The name of the cards 2 - 10
						name = _value.ToString();
						break;						
				}

				return name;
			}
		}

		/// <summary>
		/// Another example of a calculated read-only property
		/// </summary>
		public string SuitName
		{
			get { return _suit.ToString(); }
		}

		public override string ToString()
		{
			return $"{this.CardName} of {this.Suit}";
		}
	}
}
